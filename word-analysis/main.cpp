
#include <iostream>
#include <string>
#include <vector>
#include <thread>
#include <unordered_map>
#include <fstream>
#include <chrono>
#include <mutex>
#include <atomic>
#include <fstream>
#include <tbb/tbb.h>

extern "C" {
    #include "porter.c"
}

using namespace std;
using namespace tbb;

static const string filename = "out.txt";
static const int rbuffersize = 10000;
static const int numThreads = 1;//thread::hardware_concurrency();

bool is_running = true,
     is_reading = true;

std::mutex mut;

void signalHandler( int signum )
{
    is_running = false;
}

unordered_map<char, char> replacements ({
    {'.', ' '},
    {',', ' '},
    {'-', ' '},
    {'<', ' '},
    {'>', ' '},
    {',', ' '},
    {'&', ' '},
    {'@', ' '},
    {'!', ' '},
    {'[', ' '},
    {']', ' '},
    {'{', ' '},
    {'}', ' '},
    {'+', ' '},
    {'=', ' '},
    {'$', ' '},
    {'^', ' '},
    {'*', ' '},
    {'/', ' '},
    {'\\', ' '},
    {'\'', ' '},
    {'"', ' '},
    {'?', ' '},
    {'_', ' '},
    {'(', ' '},
    {')', ' '},
    {'|', ' '},
    {'#', ' '},
    {'`', ' '},
    {'~', ' '},
    {':', ' '},
    {';', ' '},
    /*{'“', ' '},
    {'‘', ' '},
    {'«', ' '},
    {'»', ' '},*/

    {'A', 'a'},
    {'B', 'b'},
    {'C', 'c'},
    {'D', 'd'},
    {'E', 'e'},
    {'F', 'f'},
    {'G', 'g'},
    {'H', 'h'},
    {'I', 'i'},
    {'J', 'j'},
    {'K', 'k'},
    {'L', 'l'},
    {'M', 'm'},
    {'N', 'n'},
    {'O', 'o'},
    {'P', 'p'},
    {'Q', 'q'},
    {'R', 'r'},
    {'S', 's'},
    {'T', 't'},
    {'U', 'u'},
    {'V', 'v'},
    {'W', 'w'},
    {'X', 'x'},
    {'Y', 'y'},
    {'Z', 'z'},

    /*{'é', 'e'},
    {'ñ', 'n'},
    {'í', 'i'},
    {'ø', 'o'},
    {'á', 'a'},
    {'å', 'a'},
    {'ü', 'u'},
    {'ö', 'o'},//*/
});

void clean_string(string * str) {
    for(int idx = 0; idx < str->size(); idx++) {
        auto replacement = replacements.find((*str)[idx]);
        if(replacement != replacements.end()){
            (*str)[idx] = replacement->second;
        }
    }
}

void count_words(concurrent_queue<string> *q, unordered_map<string, int> * wordcounts){
    string current_line, current_word;
    char cstr[256];
    struct stemmer * z =  create_stemmer();
    int stemidx;
    while(is_running) {
        if(q->try_pop(current_line)) {
            for(int idx = 0; idx < current_line.size(); idx++){
                if(current_line[idx] == ' ') {
                    mut.lock();
                    strcpy(cstr, current_word.c_str());
                    stemidx = stem(z, cstr, strlen(cstr) - 1);
                    cstr[stemidx+1] = '\0';
                    (*wordcounts)[(string)cstr] += 1;
                    current_word.clear();
                    
                    mut.unlock();
                } else {
                    mut.lock();
                        current_word += current_line[idx];
                    mut.unlock();
                }
            }
        } else {
            if(is_reading) {
                this_thread::sleep_for(chrono::microseconds(10));
            } else {
                is_running = false;
            }
        }
    }
    free_stemmer(z);
}

void buffered_read(concurrent_queue<string> * queue) {
    ifstream ifs;
    ifs.open("out.txt", ios::binary | ios::ate);
    ifs.seekg(0, ifs.end);
    int ifsize = ifs.tellg();
    ifs.seekg(0,ifs.beg);
    int idx = 0;
    vector<char> buffer;
    buffer.resize(rbuffersize);
    string nextbuffer;
    buffer.reserve(rbuffersize);
    while(is_reading) {
        if(idx + rbuffersize <= ifsize) {
            ifs.read(buffer.data(),rbuffersize);
            idx += rbuffersize;
            ifs.seekg(idx, ios_base::beg); /* Move forward by the size of the buffer */
            
            for(auto cit = buffer.begin(); cit != buffer.end(); cit++) {
                if(*cit == '\n') {
                    clean_string(&nextbuffer);
                    queue->push(nextbuffer);
                    nextbuffer.clear();
                } else {
                    nextbuffer += *cit;
                }
            }
            ifs.clear();
        } else {
            int finalbuffsize = ifsize - idx;
            ifs.read(buffer.data(), finalbuffsize);
            for(auto cit = buffer.begin(); cit != buffer.end(); cit++) {
                if(*cit == '\n') {
                    clean_string(&nextbuffer);
                    queue->push(nextbuffer);
                    nextbuffer.clear();
                } else {
                    nextbuffer += *cit;
                }
            }
            is_reading = false;
        }
    }
}

int main(int argc, char* argv[]) {
    /*setup*/
    vector<thread> threads;
    concurrent_queue<string> queue;
    
    std::atomic<int> word_count;
    word_count = 0;

    auto start = std::chrono::system_clock::now();

    /*read into queue*/
    buffered_read(&queue);

    /*setup threads*/
    unordered_map<string, int> wordcounts;
    vector<unordered_map<string,int> > document_word_counts;
    for(int x = 0; x < numThreads; x++) {
        threads.push_back(thread(count_words, &queue, &wordcounts));
    }
    
    while(is_running) {
        this_thread::sleep_for(chrono::seconds(1));
    }

    /*cleanup threads*/
    for(thread & t : threads) {
        t.join();
    }

    
    /*output*/
    for(auto & it : wordcounts) {
        if(it.second > 1000){
            //cout << it.first << "\t|\t" << it.second << endl;
        }
    }
    cout << "Total Words: " << wordcounts.size() << endl;

    if(argc > 1) {
        ofstream ofile(argv[1]);
        if(ofile.is_open()){
            for(auto & it : wordcounts) {
                ofile << it.first << "\t" << it.second << endl;
            }
        }
        ofile.close();
    }
    
    auto stop = std::chrono::system_clock::now();
    cout << "Time: " << chrono::duration_cast<chrono::duration<double> >(stop - start).count() << endl;
    return 0;
}