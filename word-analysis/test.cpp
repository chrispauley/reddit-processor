#include <string>
#include <iostream>
#include <unordered_map>

using namespace std;

int main() {
    unordered_map<string, int> test;
    test["yo"] += 1;
    cout << test["yo"] << endl;
    return 0;
}