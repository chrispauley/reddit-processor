var fs = require('fs');

var lineReader = require('readline').createInterface({
  input: fs.createReadStream('small_reddit.json')
});
var filebody = "";
lineReader.on('line', function (line) {
  var lineJSON = JSON.parse(line),
        body = lineJSON.body;
    fs.appendFileSync("out.txt",body.replace(/\n|\r/g, "") + "\n");
});