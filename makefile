CC      = g++
CFLAGS  = --std=c++14
LDFLAGS = -ltbb -lpthread

all: main clean

main: main.o
	$(CC) -o build/$@ $^ $(LDFLAGS)

main.o: main.cpp
	$(CC) -c $(CFLAGS) $<

.PHONY: clean cleanest

clean:
	rm *.o

cleanest: clean
	rm main
