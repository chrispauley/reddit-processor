#include "inc/json.hpp"

#include <tbb/tbb.h>
#include <tbb/concurrent_unordered_set.h>
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <thread>
#include <chrono>
#include <csignal>
#include <utility>

#define FILE_LOCATION "build/data/topten.json"
#define NUM_THREADS  thread::hardware_concurrency() - 1

using namespace std;
using namespace tbb;
using json = nlohmann::json;

bool is_running = true;
bool finished_reading = false;

void sighandler(int x){
    is_running = false;
}

void read_line(tbb::concurrent_queue< pair<int, string> > *lines,
                tbb::concurrent_unordered_set<string> *authors, 
                concurrent_unordered_set<pair<string, int> >  *author_indexes,
                tbb::concurrent_unordered_set<string>  *subreddits,
                concurrent_unordered_set< pair<string, int> >  *subreddit_indexes){
    pair<int, string> current_line;
    string current_author;
    string current_subreddit;
    string current_string(256,'\0');
    int current_string_iterator = 0;
    bool in_key = false;
    bool is_subreddit = false;
    int current_post;
    concurrent_unordered_map<int, concurrent_unordered_set<int> >::iterator current_author_index, current_subreddit_index;
    while(is_running){
        if(lines->try_pop(current_line)){
            /* TODO declare this outside the loop (need to know structure)*/ 
            //json js = json::parse(current_line);
            
            //current_author =  js["author"];
            //current_subreddit = js["subreddit"];
            
            current_post = current_line.first;

            // "subreddit":"
            // "author":"
            
            
            in_key = false;
            is_subreddit = false;
            //string current_string = "";
            current_string_iterator = 0;
            for (unsigned int i = 0; i < current_line.second.length(); i++) {
                char c = current_line.second[i];
                if (c == '"') {
                    if (in_key) {
                        current_string[current_string_iterator] = '\0';
                        if (is_subreddit) {
                            current_subreddit = string(current_string,0,current_string_iterator);
                        } else {
                            current_author = string(current_string,0,current_string_iterator);
                        }
                        in_key = false;
                        is_subreddit = false;
                        current_string_iterator = 0;
                        continue;
                    } else {
			if(current_line.second.length() < i + 11) {
				continue;
			}
                        if (current_line.second.substr(i+6,4) == "r\":\"") {
                            in_key = true;
                            i += 10;
                            continue;
                        } else if (current_line.second.substr(i+6,5) == "ddit\"") {
                            in_key = true;
                            is_subreddit = true;
                            i += 13;
                            continue;
                        }
                    }
                } else {
                    if (in_key) {
                        current_string[current_string_iterator++] = c;
                    }
                }
            }
            author_indexes->insert({current_author, current_post});
            subreddit_indexes->insert({current_subreddit, current_post});
            //authors->insert(current_author);
	    //subreddits->insert(current_subreddit);
        } else {
            if(finished_reading){
                is_running = false;
            }
          this_thread::sleep_for(std::chrono::microseconds(10));  
        }
        //
    }
}

int main() {
    signal(SIGINT,sighandler);
    ifstream datafile (FILE_LOCATION);
    
    tbb::concurrent_unordered_set<string> authors;
    concurrent_unordered_set<pair<string,int> > author_indexes;
    tbb::concurrent_unordered_set<string> subreddits;
    concurrent_unordered_set<pair<string,int> > subreddit_indexes;
    //concurrent_unordered_set< pair <int, int> > author_pairs, sub_pairs;
    tbb::concurrent_queue<pair<int, string> > lines;

    vector<thread> threads;
    cout << "Spin Up Threads" << endl;
    chrono::high_resolution_clock::time_point t1 = chrono::high_resolution_clock::now();
    for(unsigned int thread_index = 0; thread_index < NUM_THREADS; thread_index++){
        threads.emplace_back(thread(read_line, &lines, &authors, &author_indexes, &subreddits, &subreddit_indexes));
    }
    cout << "Read Lines" << endl;
    int linecount = 0;
    string line;
    bool is_reading = true;
    while(is_reading && is_running) {
        if(getline(datafile, line)) {
//            cout << "\rReading line: " << linecount << " queue: " << linecount - lines.unsafe_size();
            lines.push({linecount++, line});
//            linecount += 1;
        } else {
            is_reading = false;
        }
    }

    finished_reading = true;
    
    cout << endl << "LINES: " << linecount << endl;
    datafile.close();
    
    cout << "Wait for threads to finish" << endl;
    while(is_running){
//        cout << "\rOn Post: " << (linecount - lines.unsafe_size());
    }
//    read_line(&lines, &authors, &author_indexes, &subreddits, &subreddit_indexes);
    
    for(unsigned int threadit = 0; threadit < threads.size(); threadit++){
        threads.at(threadit).join();
    }


    ofstream resultfile ("users.json");
    
    resultfile << "{";
    concurrent_unordered_map<string, vector<int> > author_posts;
    for(concurrent_unordered_set<pair<string, int> >::iterator authit = author_indexes.begin(); authit != author_indexes.end(); authit++){
        author_posts[authit->first].push_back(authit->second);
    }
    for(concurrent_unordered_map<string, vector<int> >::iterator authpostit = author_posts.begin(); authpostit != author_posts.end(); authpostit++){
        resultfile << "\"" << authpostit->first << "\":[";
        for(vector<int>::iterator postit = authpostit->second.begin(); postit != authpostit->second.end(); postit++) {
            resultfile << *postit << ",";
        }
        resultfile << "0],";
    }
    resultfile << "}";


    cout << endl << "Finished processing " << (linecount - lines.unsafe_size()) << " posts" << endl;

    chrono::high_resolution_clock::time_point t2 = chrono::high_resolution_clock::now();

    cout << "Time: " << chrono::duration_cast<chrono::duration<double>>(t2 - t1).count() << endl;
/*
    ofstream resultfile ("users.json");
    
    resultfile << "{";
    
    int author_index = 0;
    cout << "USER COUNT: " << author_indexes.size() << endl;
    for(tbb::concurrent_unordered_set<string>::iterator authit = authors.begin(); authit != authors.end(); authit++){
        
        if(author_indexes.find(author_index) != author_indexes.end()){
            resultfile << "\""<<  *authit << "\" : [";
            for(concurrent_unordered_set<int>::iterator authpostit = author_indexes.find(author_index)->second.begin(); authpostit != author_indexes.find(author_index)->second.end(); authpostit++){
                resultfile << *authpostit << ", ";
            }
            resultfile << "0],";
        }
        author_index++;
    }
    resultfile << "}";
    
    resultfile.close();
    ofstream subfile ("subreddits.json");
    
    subfile << "{";
    
    int subreddit_index = 0;
    cout << "SUB COUNT: " << subreddit_indexes.size() << endl;
    for(tbb::concurrent_unordered_set<string>::iterator subit = subreddits.begin(); subit != subreddits.end(); subit++){
        
        if(subreddit_indexes.find(subreddit_index) != subreddit_indexes.end()){
            subfile << "\""<<  *subit << "\" : [";
            for(concurrent_unordered_set<int>::iterator subpostit = subreddit_indexes.find(subreddit_index)->second.begin(); subpostit != subreddit_indexes.find(subreddit_index)->second.end(); subpostit++){
                subfile << *subpostit << ", ";
            }
            subfile << "0],";
        }

        
        
        subreddit_index++;
    }
    subfile << "}";
    
    subfile.close();
*/
    
    return 0;
}
